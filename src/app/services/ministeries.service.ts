import { Observable } from 'rxjs';
import { Ministeries } from './../modals/ministeries';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class MinisteriesService {

    private postUrl = 'https://52ne3x5m99.execute-api.ap-south-1.amazonaws.com/dev/ministeries';

    constructor(private http: HttpClient) {
    }

    public fetchMinisteries(): Observable<{ ministeries: Array<Ministeries> }> {
        return this.http.get<{ ministeries: Array<Ministeries> }>(this.postUrl);
    }
}