import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { PagesModule } from './pages/pages.module';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AuthorizationService } from './services/authorization.service';
import { AuthGuardService } from './guards/auth-guard';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [
    AppComponent

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    AppRoutingModule,
    PagesModule
  ],
  providers: [AuthorizationService, AuthGuardService],
  bootstrap: [DashboardComponent]
})
export class AppModule { }
