import { AwsResponse } from './../modals/aws-response';
import { Observable } from 'rxjs';
import { BibleQuote } from './../modals/bible-quote';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class BibleQuoteService {
    private postUrl = 'https://qgwzg6waja.execute-api.ap-south-1.amazonaws.com/dev/bible-quote';
    constructor(private http: HttpClient) { }

    public saveBibleQuote(bibleQuote: BibleQuote, token): Observable<AwsResponse> {
        return this.http.post<AwsResponse>(this.postUrl, bibleQuote,
            { headers: new HttpHeaders({ Authorization: token }) });
    }
}