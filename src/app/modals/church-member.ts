export interface Member {
    name: string;
    address: string;
    mobile: string;
}

export interface ArulValviyum {
    name: string;
    number: number;
    members: Member[];
}