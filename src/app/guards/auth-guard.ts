import { AuthorizationService } from 'src/app/services/authorization.service';
import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';


@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(private _router: Router, private authService: AuthorizationService) {
    }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const isLoggedIn = this.authService.getAuthState().loggedIn;
        if (!isLoggedIn) {
            alert('You have no permission to view this age. Please Login!');
            this._router.navigate([`admin`]);
            return false;
        }
        return true;
    }

}