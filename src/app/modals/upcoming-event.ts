export interface UpcomingEvent {
    eventId?: string;
    title: string;
    description: string;
    image: string;
    date: string;
}