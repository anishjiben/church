import { ProgressBarService } from './../../services/progress-bar.service';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from 'src/app/services/authorization.service';
import { AuthState } from 'src/app/modals/auth-state';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public authState: AuthState;

  constructor(public router: Router, public authService: AuthorizationService,
    public pbService: ProgressBarService) {
    this.authService.authStateChange.subscribe(authState => {
      setTimeout(() => this.authState = authState);
    });
  }

  ngOnInit(): void {
  }
  // redirectToHome()
  // {
  //   this.router.navigate([`home`]);

  redirectToPriests() {
    this.router.navigate([`priests`]);
  }
  redirectToSisters() {
    this.router.navigate([`sisters`]);
  }
  redirectToCommittee() {
    this.router.navigate([`committee`]);
  }
  redirectToFamily() {
    this.router.navigate([`family`]);
  }


}
