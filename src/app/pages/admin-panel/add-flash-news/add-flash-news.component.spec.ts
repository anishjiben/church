import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFlashNewsComponent } from './add-flash-news.component';

describe('AddFlashNewsComponent', () => {
  let component: AddFlashNewsComponent;
  let fixture: ComponentFixture<AddFlashNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFlashNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFlashNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
