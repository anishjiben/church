import { UpcomingEvent } from './../modals/upcoming-event';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AwsResponse } from '../modals/aws-response';

@Injectable()
export class UpcomingEventService {

    private imageUrl = 'https://6wk5lkrjve.execute-api.ap-south-1.amazonaws.com/dev/image-url';
    private postUrl = 'https://5zyi07kyw0.execute-api.ap-south-1.amazonaws.com/dev/upcoming-event';
    constructor(private http: HttpClient) {
    }

    public getSignedUrl(): Observable<string[]> {
        return this.http.get<string[]>(this.imageUrl);
    }

    public uploadImage(fileToUpload: File, signedUrl: string): Observable<boolean> {
        const formData: FormData = new FormData();
        formData.append('fileKey', fileToUpload, fileToUpload.name);
        return this.http.put<boolean>(signedUrl, formData)
    }

    public saveUpcomingEvent(upcomingEvent: UpcomingEvent): Observable<AwsResponse> {
        return this.http.post<AwsResponse>(this.postUrl, upcomingEvent);
    }
}