import { Injectable } from '@angular/core';

@Injectable()
export class ProgressBarService {
    public progressBarVisible = false;
    public showProgressBar(): void {
        this.progressBarVisible = true;
    }
    public hideProgressBar(): void {
        this.progressBarVisible = false;
    }
}