import { AwsResponse } from './../modals/aws-response';
import { FlashNews } from './../modals/flash-news';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class FlashNewsService {

    private imageUrl = 'https://6wk5lkrjve.execute-api.ap-south-1.amazonaws.com/dev/image-url';
    private postUrl = 'https://kfnyh2nd8e.execute-api.ap-south-1.amazonaws.com/dev/flash-news';
    constructor(private http: HttpClient) {
    }

    public getSignedUrl(): Observable<string[]> {
        return this.http.get<string[]>(this.imageUrl);
    }

    public uploadImage(fileToUpload: File, signedUrl: string): Observable<boolean> {
        const formData: FormData = new FormData();
        formData.append('fileKey', fileToUpload, fileToUpload.name);
        return this.http.put<boolean>(signedUrl, formData)
    }

    public saveFlashNews(flashNews: FlashNews): Observable<AwsResponse> {
        return this.http.post<AwsResponse>(this.postUrl, flashNews);
    }
}