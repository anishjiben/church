import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(private router: Router,) { }

  ngOnInit(): void {
  }
  redirectToHome()
  {
    this.router.navigate([`home`]);

  }
  redirectToHistory()
  {
    this.router.navigate([`history`]);
  }

  redirectToGallery()
  {
    this.router.navigate([`gallery`]);
  }

  redirectToMinistries()
  {
    this.router.navigate([`ministries`]);

  }
  redirectToMissionaries()
  {
    this.router.navigate([`missionaries`]);
  }

  redirectToCommittee()
  {
    this.router.navigate([`committee`]);
  }
  redirectToFamily()
  {
    this.router.navigate([`family`]);
  }
  redirectToBlog()
  {
    this.router.navigate([`blog`]);
  }
  redirectToContact()
  {
    this.router.navigate([`contact`]);
  }

}
