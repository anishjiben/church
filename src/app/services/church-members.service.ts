import { ArulValviyum } from './../modals/church-member';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ChurchMembersService {

    private getUrl = 'https://cw2uem20nk.execute-api.ap-south-1.amazonaws.com/dev/church-members';
    constructor(private http: HttpClient) {
    }

    public fetchChurchMembers(): Observable<{ churchMembers: Array<ArulValviyum> }> {
        return this.http.get<{ churchMembers: Array<ArulValviyum> }>(this.getUrl);
    }
}