export interface AuthState {
    userName:string,
    loggedIn:boolean
    forcePasswordChange:boolean,
};