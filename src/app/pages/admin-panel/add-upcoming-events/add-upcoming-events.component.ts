import { UpcomingEventService } from './../../../services/upcoming-event.service';
import { UpcomingEvent } from './../../../modals/upcoming-event';
import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-add-upcoming-events',
  templateUrl: './add-upcoming-events.component.html',
  styleUrls: ['./add-upcoming-events.component.scss']
})
export class AddUpcomingEventsComponent implements OnInit {


  public imageUploadUrl: string;
  public fileToUpload: File = null;
  public upcomingEvent: UpcomingEvent;
  public spinner = false;
  public eventDate;

  constructor(private ueService: UpcomingEventService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.upcomingEvent = {
      title: '',
      description: '',
      image: '',
      date: ''
    };

  }

  public handleFileInput(files: FileList): void {
    this.fileToUpload = files.item(0);
  }

  private uploadFileActivity(): void {
    this.ueService.uploadImage(this.fileToUpload, this.imageUploadUrl).subscribe(data => {
      this.saveUpcomingEvent();
    }, error => {
      console.log(error);
    });
  }

  private saveUpcomingEvent(): void {
    this.upcomingEvent.image = this.imageUploadUrl.split('?')[0];
    this.upcomingEvent.date = this.eventDate.toString();
    this.ueService.saveUpcomingEvent(this.upcomingEvent).subscribe(
      data => { this.onSavedSuccesfully(data) },
      err => { this.onError() }
    );
  }

  private onSavedSuccesfully(data): void {
    this.messageService.add({ severity: 'success', summary: 'Success Message', detail: 'Flash News Updated Succesfully' });
    this.spinner = false;
  }

  private onError(): void {
    this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Something went wrong' });
    this.spinner = false;
  }

  public onSaveButtonClicked(): void {
    // TODO: uncomment below code once Sajin has fixed UI
    // this.spinner = true;
    // this.ueService.getSignedUrl().subscribe(url => {
    //   this.imageUploadUrl = url[0];
    //   this.uploadFileActivity();
    // });
  }
}
