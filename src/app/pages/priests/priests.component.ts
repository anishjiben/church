import { PriestService } from './../../services/priests.service';
import { Component, OnInit, HostListener } from '@angular/core';
import { Priest } from 'src/app/modals/priest';

@Component({
  selector: 'app-priests',
  templateUrl: './priests.component.html',
  styleUrls: ['./priests.component.css']
})
export class PriestsComponent implements OnInit {

  title = 'Priest';
  customOptions: any = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: false
  }

  public bishops: Priest[];
  public fathers: Priest[];
  public sisters: Priest[];
  public pope: Priest;

  constructor(private priestService: PriestService) { }

  ngOnInit() {
    this.priestService.fetchPriests().subscribe(data => {
      if (data.priests) {
        this.onSuccess(data.priests)
      } else {
        this.onError('something went wrong');
      }
    },
      this.onError.bind(this)
    );
  }

  private onSuccess(priests: Priest[]): void {
    this.bishops = priests.filter(item => {
      return item.category === 'bishop'
    });

    this.fathers = priests.filter(item => {
      return item.category === 'father'
    });

    this.sisters = priests.filter(item => {
      return item.category === 'sister'
    });
    this.pope = priests.filter(item => {
      return item.category === 'pope'
    })[0];
  }

  private onError(err): void {
    console.log('Priest component : ', err);
  }

}