export interface Ministeries {
    name: string,
    time: string,
    image: string,
    leader: string,
    numberOfMembers: number
}