export interface BibleQuote {
    chapter: string;
    sentence: string;
}