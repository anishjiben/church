import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBibleSentenceComponent } from './add-bible-sentence.component';

describe('AddBibleSentenceComponent', () => {
  let component: AddBibleSentenceComponent;
  let fixture: ComponentFixture<AddBibleSentenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBibleSentenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBibleSentenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
