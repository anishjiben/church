import { CommitteService } from './../../services/committe.service';
import { Component, OnInit } from '@angular/core';
import { CommitteMember } from 'src/app/modals/committe-member';

@Component({
  selector: 'app-committee',
  templateUrl: './committee.component.html',
  styleUrls: ['./committee.component.css']
})
export class CommitteeComponent implements OnInit {

  constructor(public cmService: CommitteService) { }

  ngOnInit(): void {
    this.cmService.fetchCommitteMembers().subscribe({
      next: this.onSuccess.bind(this),
      error: this.onError.bind(this)
    }
    );
  }

  private onSuccess(data: { committeMembers: CommitteMember[] }): void {
    if (data.committeMembers?.length > 0) {
      this.cmService.setArulvalviyamDetails(data.committeMembers);
      console.log(this.cmService.committeMembers);
    }
  }

  private onError(err): void {
    console.log('Committe members : ', err);
  }

}
