export interface CommitteMember {
    name: string,
    address: string,
    image: string,
    mobile: string,
    poruppu?: string
    arulvalviyam?: {
        name: string,
        number: string
    },
}