import { ModuleWithProviders, Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { HistoryComponent } from './history/history.component';
import { GalleryComponent } from './gallery/gallery.component';
import { MinistriesComponent } from './ministries/ministries.component';
import { PriestsComponent } from './priests/priests.component';
import { CommitteeComponent } from './committee/committee.component';
import { FamilyComponent } from './family/family.component';
import { BlogComponent } from './blog/blog.component';
import { ContactComponent } from './contact/contact.component';
import { DashboardComponent } from './dashboard/dashboard.component';



export const pagesRoutes = [
//   {
//   path: '',
//   component: DashboardComponent,
//   children: [
  
//   {
//     path: 'home',
//     component: IndexComponent,
//     data: {
//       title: 'Home - Page',
//     }
//   },
//   {
//     path: 'history',
//     component: HistoryComponent,
//     data: {
//       title: 'History',
//     }
//   },
 
//   {
//     path: 'gallery',
//     component: GalleryComponent,
//     data: {
//       title: 'Gallery',
//     }
//   },
//   {
//     path: 'ministries',
//     component: MinistriesComponent,
//     data: {
//       title: 'Ministries',
//     }
//   },
//   {
//     path: 'missionaries',
//     component: PriestsComponent,
//     data: {
//       title: 'Missionaries',
//     }
//   },    {
//     path: 'committee',
//     component: CommitteeComponent,
//     data: {
//       title: 'Committee',
//     }
//   },  {
//     path: 'family',
//     component: FamilyComponent,
//     data: {
//       title: 'Family',
//     }
//   },
//   {
//     path: 'blog',
//     component: BlogComponent,
//     data: {
//       title: 'Blog',
//     }
//   },
//   {
//     path: 'contact',
//     component: ContactComponent,
//     data: {
//       title: 'Contact',
//     }
//   },
// ]}
  
];

export const pagesRouting: ModuleWithProviders = RouterModule.forChild(
  pagesRoutes
);
