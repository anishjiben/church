import { FlashNews } from './../../../modals/flash-news';
import { FlashNewsService } from './../../../services/flash-news.service';
import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-add-flash-news',
  templateUrl: './add-flash-news.component.html',
  styleUrls: ['./add-flash-news.component.scss']
})
export class AddFlashNewsComponent implements OnInit {

  public imageUploadUrl: string;
  public fileToUpload: File = null;
  public flashNews: FlashNews;
  public spinner = false;

  constructor(private fnService: FlashNewsService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.flashNews = {
      title: '',
      description: '',
      image: ''
    };

  }

  public handleFileInput(files: FileList): void {
    this.fileToUpload = files.item(0);
  }

  private uploadFileActivity(): void {
    this.fnService.uploadImage(this.fileToUpload, this.imageUploadUrl).subscribe(data => {
      this.saveFlashNews();
    }, error => {
      console.log(error);
    });
  }

  private saveFlashNews(): void {
    this.flashNews.image = this.imageUploadUrl.split('?')[0];
    this.fnService.saveFlashNews(this.flashNews).subscribe(
      data => { this.onSavedSuccesfully(data) },
      err => { this.onError() }
    );
  }

  private onSavedSuccesfully(data): void {
    this.messageService.add({ severity: 'success', summary: 'Success Message', detail: 'Flash News Updated Succesfully' });
    this.spinner = false;
  }

  private onError(): void {
    this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Something went wrong' });
  }

  public onSaveButtonClicked(): void {
    // TODO: uncomment below code once Sajin has fixed UI
    // this.spinner = true;
    // this.fnService.getSignedUrl().subscribe(url => {
    //   this.imageUploadUrl = url[0];
    //   this.uploadFileActivity();
    // });
  }

  public isFormValid(): boolean {
    if (this.flashNews.image.length < 1 || this.flashNews.title.length < 1 ||
      this.flashNews.description.length < 1) {
      return false;
    } else {
      return true;
    }
  }

}
