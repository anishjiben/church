import { CommitteMember } from './../modals/committe-member';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CommitteService {

    private postUrl = 'https://m7vzy3c5j4.execute-api.ap-south-1.amazonaws.com/dev/committe-members';
    public committeMembers = {
        arulvalviyamDetails: [],
        parishPorupalargal: []
    };

    constructor(private http: HttpClient) {
    }

    public fetchCommitteMembers(): Observable<{ priests: Array<CommitteMember> }> {
        return this.http.get<{ priests: Array<CommitteMember> }>(this.postUrl);
    }

    public setArulvalviyamDetails(avDetails: CommitteMember[]): void {
        this.committeMembers.arulvalviyamDetails = avDetails.filter(member => {
            return !member.hasOwnProperty('poruppu');
        });
        this.committeMembers.parishPorupalargal = avDetails.filter(member => {
            return member.hasOwnProperty('poruppu');
        });
    }
}