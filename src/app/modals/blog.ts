export interface Blog {
    image: string;
    pdf: string;
    author: string;
    name: string;
    description: string;
}