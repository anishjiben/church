import { MinisteriesService } from './../../services/ministeries.service';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Ministeries } from 'src/app/modals/ministeries';

@Component({
  selector: 'app-ministries',
  templateUrl: './ministries.component.html',
  styleUrls: ['./ministries.component.css']
})
export class MinistriesComponent implements OnInit {

  public ministeries: Ministeries[] = [];

  constructor(private ministeriesService: MinisteriesService) { }

  ngOnInit(): void {
    this.getMinisteriesDetails();
  }

  getMinisteriesDetails()
  {
    this.ministeriesService.fetchMinisteries().subscribe(data => {
      if (data?.ministeries.length > 0) {
        this.ministeries = data.ministeries;
        console.log(this.ministeries);
      } else {
        this.onError('somthing went wrong');
      }
    },
      this.onError.bind(this));
  }
  onError(err) {
    console.log('Ministeries error : ', err);
  }

}


