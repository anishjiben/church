import { ChurchMembersService } from './../../services/church-members.service';
import { ArulValviyum } from './../../modals/church-member';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-family',
  templateUrl: './family.component.html',
  styleUrls: ['./family.component.css']
}
)
export class FamilyComponent implements OnInit {

  public churchMembers: ArulValviyum[] = [];

  constructor(private cmService: ChurchMembersService) { }

  ngOnInit(): void {
    this.cmService.fetchChurchMembers().subscribe(data => {
      if (data?.churchMembers.length > 0) {
        data.churchMembers.sort((a, b) => {
          return a.number - b.number;
        });
        this.churchMembers = data.churchMembers;
        console.log(this.churchMembers);
      } else {
        this.onError('somthing went wrong');
      }
    },
      this.onError.bind(this));
  }

  onError(err) {
    console.log('FamilyComponent error : ', err);
  }

}