import { BibleQuoteService } from './../../../services/bible-quote.service';
import { BibleQuote } from './../../../modals/bible-quote';
import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { AuthorizationService } from 'src/app/services/authorization.service';

@Component({
  selector: 'app-add-bible-sentence',
  templateUrl: './add-bible-sentence.component.html',
  styleUrls: ['./add-bible-sentence.component.scss']
})
export class AddBibleSentenceComponent implements OnInit {

  public bibleQuote: BibleQuote;
  public spinner = false;

  constructor(private bqService: BibleQuoteService, private messageService: MessageService,
    private authService:AuthorizationService) {
    this.bibleQuote = {
      chapter: '',
      sentence: ''
    };
  }

  ngOnInit(): void {
  }

  public saveBibleQuote(): void {
    this.authService.getAuthenticatedUser().getSession((err, session)=>{
      if(err){
        return;
      }
      this.bqService.saveBibleQuote(this.bibleQuote, session.getIdToken().getJwtToken()).subscribe(
        data => { this.onSavedSuccesfully(data) },
        err => { this.onError() }
      );
    });
  }

  private onSavedSuccesfully(data): void {
    this.messageService.add({ severity: 'success', summary: 'Success Message', detail: 'Flash News Updated Succesfully' });
    this.spinner = false;
  }

  private onError(): void {
    this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Something went wrong' });
  }
}
