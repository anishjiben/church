import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationDetails, CognitoUser, CognitoUserAttribute, CognitoUserPool, CognitoUserSession } from 'amazon-cognito-identity-js';
import { BehaviorSubject } from 'rxjs';
import { AuthState } from '../modals/auth-state';

const POOL_DATA = {
    UserPoolId: 'ap-south-1_6AgRDI2Cd',
    ClientId: '13ditcpekfkq4gcfg52esu6q33'
};
const userPool = new CognitoUserPool(POOL_DATA);
@Injectable()
export class AuthorizationService {
    private authState: AuthState = {
        userName: 'anonymous',
        loggedIn: false,
        forcePasswordChange: false
    };
    private cognitoUser: CognitoUser;
    public authStateChange: BehaviorSubject<AuthState> = new BehaviorSubject<AuthState>(this.authState);

    constructor(private router: Router) {
        this.getAuthenticatedUser()?.getSession((err, session) => {
            if (err) {

            }
            else if (session.isValid()) {
                const userName = this.getAuthenticatedUser().getUsername();
                this.authState = this.getAuthStateObject(userName, false, true);
                this.authStateChange.next(this.authState);
            }
        });
    }

    public login(userName: string, password: string): void {
        const authData = {
            Username: userName,
            Password: password
        };
        const authDetails = new AuthenticationDetails(authData);
        const userData = {
            Username: userName,
            Pool: userPool
        };
        this.cognitoUser = new CognitoUser(userData);
        const that = this;
        this.cognitoUser.authenticateUser(authDetails, {
            onSuccess(result: CognitoUserSession) { that.onLoginSuccessfully(result); },
            newPasswordRequired(userAttributes: CognitoUserAttribute) {
                that.authState = that.getAuthStateObject(userName, true, false);
                that.authStateChange.next(that.authState);
            },
            onFailure(err) { that.onLoginFailure(err, userName); }
        });
    }

    public newPasswordChallenge(password: string, userEmail: string): void {
        const that = this;
        this.cognitoUser.completeNewPasswordChallenge(password, { email: userEmail }, {
            onSuccess(result: CognitoUserSession) { that.onLoginSuccessfully(result) },
            onFailure(err) { that.onLoginFailure(err, that.authState.userName) }
        });
    }

    private onLoginSuccessfully(result: CognitoUserSession): void {
        this.authState = this.getAuthStateObject(result.getAccessToken().payload.username, false, true);
        this.authStateChange.next(this.authState);
        this.router.navigate(['home']);
    }

    private onLoginFailure(err, userName: string): void {
        if (!this.authState.forcePasswordChange) {
            this.authState.userName = userName;
            this.authState.loggedIn = false;
            this.authState.forcePasswordChange = false;
            this.authStateChange.next(this.authState);
        }
    }

    private getAuthStateObject(userName: string, forcePswrdChange: boolean, loggedIn: boolean): AuthState {
        return {
            userName: userName,
            forcePasswordChange: forcePswrdChange,
            loggedIn: loggedIn
        };
    }

    public getAuthenticatedUser(): CognitoUser {
        return userPool.getCurrentUser();
    }
    public logout(): void {
        this.getAuthenticatedUser().signOut();
        this.authState = {
            userName: 'anonymous',
            loggedIn: false,
            forcePasswordChange: false
        };
        this.authStateChange.next(this.authState);
        this.router.navigate(['home']);
    }
    public getAuthState(): AuthState {
        return this.authState;
    }
}