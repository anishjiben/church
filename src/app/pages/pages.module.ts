import { MessagesService } from 'src/app/services/messages.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ProgressBarService } from './../services/progress-bar.service';
import { BlogService } from './../services/blog.service';
import { ChurchMembersService } from './../services/church-members.service';
import { CommitteService } from './../services/committe.service';
import { PriestService } from './../services/priests.service';
import { MinisteriesService } from './../services/ministeries.service';
import { BibleQuoteService } from './../services/bible-quote.service';
import { UpcomingEventService } from './../services/upcoming-event.service';
import { FlashNewsService } from './../services/flash-news.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { AlertModule } from 'ngx-bootstrap/alert';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
// import { CarouselModule } from "ngx-bootstrap/carousel";
import { ModalModule } from 'ngx-bootstrap/modal';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';
import { PopoverModule } from 'ngx-bootstrap/popover';

import { IndexComponent } from './index/index.component';
import { HistoryComponent } from './history/history.component';
import { FooterComponent } from './footer/footer.component';

import { HeaderComponent } from './header/header.component';
import { pagesRouting } from './pages-routing.module';
import { GalleryComponent } from './gallery/gallery.component';
import { MinistriesComponent } from './ministries/ministries.component';
import { PriestsComponent } from './priests/priests.component';

import { CommitteeComponent } from './committee/committee.component';
import { FamilyComponent } from './family/family.component';
import { BlogComponent } from './blog/blog.component';
import { ContactComponent } from './contact/contact.component';

import { DashboardComponent } from './dashboard/dashboard.component';

import { AccordionModule } from 'ngx-bootstrap/accordion';
// import { CarouselModule } from 'primeng/carousel';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { LoginComponent } from './login/login.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { AddFlashNewsComponent } from './admin-panel/add-flash-news/add-flash-news.component';
import { AddUpcomingEventsComponent } from './admin-panel/add-upcoming-events/add-upcoming-events.component';
import { AddBibleSentenceComponent } from './admin-panel/add-bible-sentence/add-bible-sentence.component';
import { DropdownModule } from 'primeng/dropdown';
import { ProgressSpinnerModule } from 'primeng/progressspinner';

import { MessageService } from 'primeng/api/';
import { CalendarModule } from 'primeng/calendar';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { DialogModule } from 'primeng/dialog';
import { ProgressBarModule } from 'primeng/progressbar';
import { MessagesModule } from 'primeng/messages';




@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    RouterModule,
    BsDropdownModule.forRoot(),
    ProgressbarModule.forRoot(),
    TooltipModule.forRoot(),
    PopoverModule.forRoot(),
    CollapseModule.forRoot(),
    JwBootstrapSwitchNg2Module,
    TabsModule.forRoot(),
    PaginationModule.forRoot(),
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),

    ModalModule.forRoot(),
    pagesRouting,
    AccordionModule.forRoot(),
    NzCollapseModule,
    DropdownModule,
    ProgressSpinnerModule,
    CalendarModule,
    CarouselModule,
    DialogModule,
    ProgressBarModule,
    NgxSpinnerModule,
    MessagesModule
  ],
  declarations: [
    IndexComponent,
    HistoryComponent,
    FooterComponent,
    HeaderComponent,
    GalleryComponent,
    MinistriesComponent,
    PriestsComponent,

    CommitteeComponent,
    FamilyComponent,
    BlogComponent,
    ContactComponent,
    DashboardComponent,
    LoginComponent,
    AdminPanelComponent,
    AddFlashNewsComponent,
    AddUpcomingEventsComponent,
    AddBibleSentenceComponent
  ],
  exports: [
    IndexComponent,
    HeaderComponent,
    FooterComponent
  ],
  providers: [FlashNewsService, MessageService, UpcomingEventService, BibleQuoteService,
    MinisteriesService, PriestService, CommitteService, ChurchMembersService, BlogService,
    ProgressBarService, MessagesService],
  schemas:
    [CUSTOM_ELEMENTS_SCHEMA]
})
export class PagesModule { }
