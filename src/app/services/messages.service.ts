import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';

@Injectable()
export class MessagesService {
    public msgs = [];
    constructor(private messageService: MessageService) {

    }

    showError() {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: 'Something went wrong! Please refresh' });
    }
    clear() {
        this.messageService.clear();
    }
}