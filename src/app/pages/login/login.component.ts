import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from 'src/app/services/authorization.service';
import { AuthState } from 'src/app/modals/auth-state';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss', './login.component.css']
})
export class LoginComponent implements OnInit {

  public userName: string;
  public email: string;
  public password: string;
  public authState: AuthState;

  constructor(private authService: AuthorizationService) {
    this.authService.authStateChange.subscribe(this.onAuthStateChange.bind(this));
  }

  ngOnInit(): void {
  }
  public login() {
    // please dont remove this code
     if(this.authState.forcePasswordChange){
       this.authService.newPasswordChallenge(this.password, this.email);
     } else {
      this.authService.login(this.userName, this.password);
    }
  }

  private onAuthStateChange(authState: AuthState): void {
    this.authState = authState;
  }
}
