import { Component, OnInit, OnDestroy, TemplateRef  } from "@angular/core";
import noUiSlider from "nouislider";
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';


@Component({
  selector: "app-index",
  templateUrl: "index.component.html",
  styleUrls: ['./index.component.css', './index.component.scss']
})
export class IndexComponent implements OnInit, OnDestroy {
  modalRef: BsModalRef;
  isCollapsed = true;
  focus;
  focus1;
  focus2;
  date = new Date();
  pagination = 3;
  pagination1 = 1;
  constructor(private router: Router,
    private modalService: BsModalService) {}
    
  scrollToDownload(element: any) {
    element.scrollIntoView({ behavior: "smooth" });
  }
  ngOnInit() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.add("index-page");

    var slider = document.getElementById("sliderRegular");

    noUiSlider.create(slider, {
      start: 40,
      connect: false,
      range: {
        min: 0,
        max: 100
      }
    });

    var slider2 = document.getElementById("sliderDouble");

    noUiSlider.create(slider2, {
      start: [20, 60],
      connect: true,
      range: {
        min: 0,
        max: 100
      }
    });
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("index-page");
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  redirectToHistory()
  {
    this.router.navigate([`history`]);
  }
  redirectToMinistries()
  {
    this.router.navigate([`ministries`]);
  }
  redirectToMissionaries()
  {
    this.router.navigate([`missionaries`]);
  }
  redirectToBlog()
  {
    this.router.navigate([`blog`]);
  }

  
}
