import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { Routes, RouterModule } from "@angular/router";

import { IndexComponent } from "./pages/index/index.component";
import { ContactComponent } from './pages/contact/contact.component';
import { GalleryComponent } from './pages/gallery/gallery.component';
import { HistoryComponent } from './pages/history/history.component';
import { MinistriesComponent } from './pages/ministries/ministries.component';
import { PriestsComponent } from './pages/priests/priests.component';
import { CommitteeComponent } from './pages/committee/committee.component';
import { FamilyComponent } from './pages/family/family.component';
import { BlogComponent } from './pages/blog/blog.component';
import { LoginComponent } from './pages/login/login.component';
import { AdminPanelComponent } from './pages/admin-panel/admin-panel.component';
import { AuthGuardService } from './guards/auth-guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: IndexComponent,
    data: {
      title: 'Nadaikkavu - Church',
    }
  },
  {
    path: 'admin',
    component: LoginComponent
  },
  {
    path: 'history',
    component: HistoryComponent,
    data: {
      title: 'History',
    }
  },

  {
    path: 'gallery',
    component: GalleryComponent,
    data: {
      title: 'Gallery',
    }
  },
  {
    path: 'ministries',
    component: MinistriesComponent,
    data: {
      title: 'Ministries',
    }
  },
  {
    path: 'missionaries',
    component: PriestsComponent,
    data: {
      title: 'Missionaries',
    }
  }, {
    path: 'committee',
    component: CommitteeComponent,
    data: {
      title: 'Committee',
    }
  }, {
    path: 'family',
    component: FamilyComponent,
    data: {
      title: 'Family',
    }
  },
  {
    path: 'blog',
    component: BlogComponent,
    data: {
      title: 'Blog',
    }
  },
  {
    path: 'contact',
    component: ContactComponent,
    data: {
      title: 'Nadaikkavu - Contact',
    }
  },
  {
    path: 'nadaikkavu',
    loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule),
    data: {
      title: 'Nadaikkavu Church',
    }
  },
  {
    path: 'admin-panel',
    component: AdminPanelComponent,
    canActivate: [AuthGuardService],
    data: {
      title: 'Administartion',
    }
  },
  { path: '**', component: ContactComponent }

];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: []
})
export class AppRoutingModule { }
