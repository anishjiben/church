import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUpcomingEventsComponent } from './add-upcoming-events.component';

describe('AddUpcomingEventsComponent', () => {
  let component: AddUpcomingEventsComponent;
  let fixture: ComponentFixture<AddUpcomingEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUpcomingEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUpcomingEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
