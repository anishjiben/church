import { NgxSpinnerService } from 'ngx-spinner';
import { BlogService } from './../../services/blog.service';
import { Component, OnInit } from '@angular/core';
import { Blog } from 'src/app/modals/blog';
import { MessagesService } from 'src/app/services/messages.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

  public blogs: Blog[] = [];
  constructor(private blogService: BlogService,
    private spinner: NgxSpinnerService, private msgService: MessagesService) { }

  ngOnInit(): void {
    this.spinner.show();
    this.blogService.fetchBlogs().subscribe(data => {
      if (data?.blogs.length > 0) {
        this.blogs = data.blogs;
        this.spinner.hide();
      } else {
        this.onError('somthing went wrong');
      }
    },
      this.onError.bind(this));
  }

  private onError(err): void {
    this.msgService.showError();
  }

}
