export interface Priest {
    name: string;
    year: string;
    category: string;
    image: string;
}