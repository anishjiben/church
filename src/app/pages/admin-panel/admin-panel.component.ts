import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api/selectitem';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.scss']
})
export class AdminPanelComponent implements OnInit {

  containers: SelectItem[];
  selectedContainer: number;

  constructor() { }

  ngOnInit(): void {
    this.containers = [
      { label: 'Select Container', value: null },
      { label: 'News', value: 1},
      { label: 'Upcoming Event', value: 2},
      { label: 'Bible Quote', value: 3}
    ];
  }


}
