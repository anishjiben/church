export interface FlashNews {
    newsId?:string;
    title:string;
    description:string;
    image:string
}