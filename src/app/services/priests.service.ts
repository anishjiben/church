import { Priest } from './../modals/priest';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class PriestService {
    private postUrl = 'https://0zcpb34es6.execute-api.ap-south-1.amazonaws.com/dev/priests';

    constructor(private http: HttpClient) {
    }

    public fetchPriests(): Observable<{ priests: Array<Priest> }> {
        return this.http.get<{ priests: Array<Priest> }>(this.postUrl);
    }
}