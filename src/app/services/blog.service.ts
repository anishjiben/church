import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Blog } from '../modals/blog';

@Injectable()
export class BlogService {
    private getUrl = 'https://6f0gt711k4.execute-api.ap-south-1.amazonaws.com/dev/blog';

    constructor(private http: HttpClient) {
    }

    public fetchBlogs(): Observable<{ blogs: Array<Blog> }> {
        return this.http.get<{ blogs: Array<Blog> }>(this.getUrl);
    }
}